package com.epam.model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueueCommunication {
    private BlockingQueue<Character> blockingQueue;
    private Thread senderThread;
    private Thread receiverThread;

    public BlockingQueueCommunication() {
        blockingQueue = new PriorityBlockingQueue<>();
        senderThread = new Thread(() -> send());
        receiverThread = new Thread(() -> receive());
    }

    public void startCommunication() {
        senderThread.start();
        receiverThread.start();
        try {
            senderThread.join();
            receiverThread.interrupt();
            receiverThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void send() {
        try {
            for (char i = 'A'; i <= 'Z'; i++) {
                Thread.sleep(1_000);
                blockingQueue.put(i);
                System.out.println("sent " + i);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("sender has finished work;");
        }
    }

    private void receive() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Thread.sleep(1_000);
                System.out.println("received " + blockingQueue.take());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
