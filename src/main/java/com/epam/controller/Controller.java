package com.epam.controller;

import com.epam.model.BlockingQueueCommunication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    public void startBlockingQueueCommunication(){
        BlockingQueueCommunication blockingQueueCommunication = new BlockingQueueCommunication();
        blockingQueueCommunication.startCommunication();
        LOGGER.info("start");
    }
}
