package com.epam.view;

import com.epam.controller.Controller;
import com.epam.view.menuServices.Menu;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {
    private Controller controller;

    public MainMenu() {
        controller = new Controller();
    }

    @Override
    public String initNameOfMenu() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<String, String>() {
            {
                put("1", "1\tBlocking Queue");
                put("Q", "Q\texit");
            }
        };
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<String, Runnable>() {
            {
                put("1", () -> controller.startBlockingQueueCommunication());
                put("Q", () -> System.out.println("\nBye"));
            }
        };
    }
}
