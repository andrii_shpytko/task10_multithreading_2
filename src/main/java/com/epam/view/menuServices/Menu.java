package com.epam.view.menuServices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public interface Menu {
    String initNameOfMenu();

    Map<String, String> initDisplay();

    Map<String, Runnable> initExecution();

    default void launch() {
        String nameOfMenu = initNameOfMenu();
        Map<String, String> items = initDisplay();
        Map<String, Runnable> menu = initExecution();
        runMenu(nameOfMenu, items, menu);
    }

    default void runMenu(String nameOfMenu, Map<String, String> items, Map<String, Runnable> menu) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String keyMenu = "";
        do {
            System.out.println("\n" + nameOfMenu);
            items.values()
                    .forEach((String item) -> System.out.println(" " + item));
            System.out.print("\nSelect menu item: ");
            try {
                keyMenu = br.readLine().toUpperCase();
                menu.get(keyMenu).run();
            } catch (IOException e) {
                e.printStackTrace();
            } catch(NullPointerException e){
                e.printStackTrace();
                System.out.println("\nInvalid input; please re-enter");
            }
        } while (!"Q".equals(keyMenu));
    }
}
