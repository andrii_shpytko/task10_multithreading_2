package com.epam.view.menuServices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IntupViewInterface {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static int integer(String line){
        while(true){
            System.out.print(line + ": ");
            try {
                return Integer.parseInt(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
